export class FilterModel {
    termilanId: number;
    yearId: number;
    monthId: number;

    constructor(termilanId, yearId, monthId) {
       this.termilanId = termilanId;
       this.yearId = yearId;
       this.monthId = monthId;
    }
}