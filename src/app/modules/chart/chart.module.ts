import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartComponent } from './chart.component';
import { ChartRoutingModule } from './chart.routing';
import { ChartsModule, ThemeService } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    ChartRoutingModule,
    ChartsModule
  ],
  declarations: [
    ChartComponent,
  ],
  providers: [ThemeService]
})
export class ChartModule { }
