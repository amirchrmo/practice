import { Component, OnDestroy, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
import { Subscription } from 'rxjs';
import { ChartModel } from 'src/app/models/chart.model';
import { ApiService } from 'src/app/service/api.service';


@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, OnDestroy {

  myChart: Chart;
  subscription: Subscription;
  chartDataModel = new ChartModel();

  // تنظیمات چارت
  chartOption = {
    type: 'line',
    data: {
        labels: [],
        datasets: [
          {
            label: 'تراکنش روزانه',
            data: [],
            backgroundColor: 'rgba(178,220,180,1)',
            borderColor: 'rgb(100, 187, 108)',
            pointBackgroundColor: 'transparent',
            pointBorderColor: 'rgb(125,181,95)',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgb(125,181,95)',
            borderWidth: 1
          },
          {
            label: 'حد انتظار ماه',
            data: [],
            backgroundColor: 'transparent',
            borderColor: 'rgb(40,154,243)',
            pointBackgroundColor: 'rgb(81,178,203)',
            pointBorderColor: 'rgb(81,178,203)',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgb(81,178,203)',
            borderWidth: 1
          },
          {
            label: 'حد انتظار هفتگی',
            data: [],
            backgroundColor: 'transparent',
            borderColor: 'rgb(252,178,41)',
            pointBackgroundColor: 'transparent',
            pointBorderColor: 'rgb(238,193,92)',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgb(238,193,92)',
            borderWidth: 3
          },
          {
            label: 'تراکنش تجمیعی',
            data: [],
            backgroundColor: 'transparent',
            borderColor: 'rgb(244,80,75)',
            pointBackgroundColor: 'rgb(194,110,110)',
            pointBorderColor: 'rgb(194,110,110)',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgb(194,110,110)',
            borderWidth: 1
          },
      ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        legend : {
          labels : {
            fontColor: 'black',
            fontSize: 14,
            fontFamily: 'Shabnam',
            usePointStyle: true,
          }
        }
    },
  };


  constructor(private apiService: ApiService) {

    // content-layout گرفتن دیتا ها از کمپوننت
    this.subscription = this.apiService.subject.subscribe((chartData: ChartModel) => {
      this.chartDataModel = chartData;
      this.chartOption.data.labels = this.chartDataModel.date;
      this.chartOption.data.datasets[2].data = this.chartDataModel.chartData;
      this.myChart = new Chart('myChart', this.chartOption);
    });

  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
