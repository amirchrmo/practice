import { Component, OnInit } from '@angular/core';
import { MonthsEnum } from 'src/app/enums/months.enum';
import { TerminalEnum } from 'src/app/enums/terminal.enum';
import { YearsEnum } from 'src/app/enums/years.enum';
import { ChartModel } from 'src/app/models/chart.model';
import { FilterModel } from 'src/app/models/filter.model';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-content-layout',
  templateUrl: './content-layout.component.html',
  styleUrls: ['./content-layout.component.scss']
})
export class ContentLayoutComponent implements OnInit {

  chartData = new ChartModel(); // مدل دیتا های چارت جهت فرستادن به کمپوننت چارت
  filterModel = new FilterModel(4, 1, 1); // پر کردن فیلتر ها به صورت پبش فرض در اولین اجرای برنامه (نمایش همه - سال98 - مهر)

  // لیست پایانه ها جهت نمایش در منوی فیلترها
  Terminals: any = [
    {
      id: 1,
      name: 'پایانه1',
    },
    {
      id: 2,
      name: 'پایانه2',
    },
    {
      id: 3,
      name: 'پایانه3',
    },
    {
      id: 4,
      name: 'نمایش همه',
    },
  ];

  // لیست سال ها جهت نمایش در منوی فیلترها
  Years: any = [
    {
      id: 1,
      name: 'سال98',
    },
    {
      id: 2,
      name: 'سال99',
    }
  ];

  // لیست ماه های پاییز جهت نمایش در منوی فیلتر ها
  Month: any = [
    {
      id: 1,
      name: 'مهر',
    },
    {
      id: 2,
      name: 'آبان',
    },
    {
      id: 3,
      name: 'آذر',
    }
  ];

  TerminalType: typeof TerminalEnum = TerminalEnum; // نوع پایانه
  YearsEnum: typeof YearsEnum = YearsEnum; // سال
  MonthsEnum: typeof MonthsEnum = MonthsEnum; // ماه

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getChartData();
  }

  // گرفتن دیتا های چارت
  getChartData() {
    this.apiService.getChartData().subscribe( (res: any) => {
      this.setFilter(res);
    });
  }

  // اعمال فیلتر ها و فرستادن دیتا ها به کمپوننت چارت
  setFilter(data) {

    if (this.filterModel.termilanId === TerminalEnum.ShowAll) { // نمایش همه دیتا ها

      if (this.filterModel.yearId === YearsEnum.navadohasht) { // نمایش همه ی دیتا های سال 98
        if (this.filterModel.monthId === MonthsEnum.Mehr) {  // نمایش همه برای مهر 98
          this.chartData = new ChartModel();
          let years = data[3].Year;
          let month = years[0].month;
          this.chartData.date = month[0].date;
          this.chartData.chartData = month[0].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.aban) {  // نمایش همه برای آبان 98
          this.chartData = new ChartModel();
          let years = data[3].Year;
          let month = years[0].month;
          this.chartData.date = month[1].date;
          this.chartData.chartData = month[1].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.azar) {  // نمایش همه برای آذر 98
          this.chartData = new ChartModel();
          let years = data[3].Year;
          let month = years[0].month;
          this.chartData.date = month[2].date;
          this.chartData.chartData = month[2].chartData;
          this.apiService.subject.next(this.chartData);

        }
      }
      else if (this.filterModel.yearId === YearsEnum.navadonoh) { // نمایش همه دیتا های سال 98

        if (this.filterModel.monthId === MonthsEnum.Mehr) {  // نمایش همه برای مهر 99
          this.chartData = new ChartModel();
          let years = data[3].Year;
          let month = years[1].month;
          this.chartData.date = month[0].date;
          this.chartData.chartData = month[0].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.aban) {  // نمایش همه برای آبان 99
          this.chartData = new ChartModel();
          let years = data[3].Year;
          let month = years[1].month;
          this.chartData.date = month[1].date;
          this.chartData.chartData = month[1].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.azar) {  // نمایش همه برای آذر 99
          this.chartData = new ChartModel();
          let years = data[3].Year;
          let month = years[1].month;
          this.chartData.date = month[2].date;
          this.chartData.chartData = month[2].chartData;
          this.apiService.subject.next(this.chartData);

        }
      }
    }
    else if (this.filterModel.termilanId === TerminalEnum.Terminal1) { // نمایش دیتا های پایانه 1

      if (this.filterModel.yearId === YearsEnum.navadohasht) { // نمایش دیتا های پایانه 1 سال 98
        if (this.filterModel.monthId === MonthsEnum.Mehr) {  // نمایش دیتا های پایانه 1 برای مهر 98
          this.chartData = new ChartModel();
          let years = data[0].Year;
          let month = years[0].month;
          this.chartData.date = month[0].date;
          this.chartData.chartData = month[0].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.aban) {  // نمایش دیتا های پایانه 1 برای آبان 98
          this.chartData = new ChartModel();
          let years = data[0].Year;
          let month = years[0].month;
          this.chartData.date = month[1].date;
          this.chartData.chartData = month[1].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.azar) {  // نمایش دیتا های پایانه 1 برای آذر 98
          this.chartData = new ChartModel();
          let years = data[0].Year;
          let month = years[0].month;
          this.chartData.date = month[2].date;
          this.chartData.chartData = month[2].chartData;
          this.apiService.subject.next(this.chartData);

        }
      }
      else if (this.filterModel.yearId === YearsEnum.navadonoh) { // نمایش دیتا های پایانه1 سال 98

        if (this.filterModel.monthId === MonthsEnum.Mehr) {  // نمایش دیتا های پایانه1 برای مهر 99
          this.chartData = new ChartModel();
          let years = data[0].Year;
          let month = years[1].month;
          this.chartData.date = month[0].date;
          this.chartData.chartData = month[0].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.aban) {  // نمایش دیتا های پایانه1 برای آبان 99
          this.chartData = new ChartModel();
          let years = data[0].Year;
          let month = years[1].month;
          this.chartData.date = month[1].date;
          this.chartData.chartData = month[1].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.azar) {  // نمایش دیتا های پایانه1 برای آذر 99
          this.chartData = new ChartModel();
          let years = data[0].Year;
          let month = years[1].month;
          this.chartData.date = month[2].date;
          this.chartData.chartData = month[2].chartData;
          this.apiService.subject.next(this.chartData);

        }
      }
    }
    else if (this.filterModel.termilanId === TerminalEnum.Terminal2) { // نمایش دیتاهای پایانه2

      if (this.filterModel.yearId === YearsEnum.navadohasht) { // نمایش دیتا های پایانه 2 سال 98
        if (this.filterModel.monthId === MonthsEnum.Mehr) {  // نمایش دیتا های پایانه 1 برای مهر 98
          this.chartData = new ChartModel();
          let years = data[1].Year;
          let month = years[0].month;
          this.chartData.date = month[0].date;
          this.chartData.chartData = month[0].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.aban) {  // نمایش دیتا های پایانه 1 برای آبان 98
          this.chartData = new ChartModel();
          let years = data[1].Year;
          let month = years[0].month;
          this.chartData.date = month[1].date;
          this.chartData.chartData = month[1].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.azar) {  // نمایش دیتا های پایانه 1 برای آذر 98
          this.chartData = new ChartModel();
          let years = data[1].Year;
          let month = years[0].month;
          this.chartData.date = month[2].date;
          this.chartData.chartData = month[2].chartData;
          this.apiService.subject.next(this.chartData);

        }
      }
      else if (this.filterModel.yearId === YearsEnum.navadonoh) { // نمایش دیتا های پایانه2 سال 98

        if (this.filterModel.monthId === MonthsEnum.Mehr) {  // نمایش دیتا های پایانه1 برای مهر 99
          this.chartData = new ChartModel();
          let years = data[1].Year;
          let month = years[1].month;
          this.chartData.date = month[0].date;
          this.chartData.chartData = month[0].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.aban) {  // نمایش دیتا های پایانه1 برای آبان 99
          this.chartData = new ChartModel();
          let years = data[1].Year;
          let month = years[1].month;
          this.chartData.date = month[1].date;
          this.chartData.chartData = month[1].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.azar) {  // نمایش دیتا های پایانه1 برای آذر 99
          this.chartData = new ChartModel();
          let years = data[1].Year;
          let month = years[1].month;
          this.chartData.date = month[2].date;
          this.chartData.chartData = month[2].chartData;
          this.apiService.subject.next(this.chartData);

        }
      }
    }
    else if (this.filterModel.termilanId === TerminalEnum.Terminal3) { // نمایش دیتاهای پایانه3

      if (this.filterModel.yearId === YearsEnum.navadohasht) { // نمایش دیتا های پایانه 3 سال 98
        if (this.filterModel.monthId === MonthsEnum.Mehr) {  // نمایش دیتا های پایانه 3 برای مهر 98
          this.chartData = new ChartModel();
          let years = data[2].Year;
          let month = years[0].month;
          this.chartData.date = month[0].date;
          this.chartData.chartData = month[0].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.aban) {  // نمایش دیتا های پایانه3 برای آبان 98
          this.chartData = new ChartModel();
          let years = data[2].Year;
          let month = years[0].month;
          this.chartData.date = month[1].date;
          this.chartData.chartData = month[1].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.azar) {  // نمایش دیتا های پایانه3 برای آذر 98
          this.chartData = new ChartModel();
          let years = data[2].Year;
          let month = years[0].month;
          this.chartData.date = month[2].date;
          this.chartData.chartData = month[2].chartData;
          this.apiService.subject.next(this.chartData);

        }
      }
      else if (this.filterModel.yearId === YearsEnum.navadonoh) { // نمایش دیتا های پایانه3 سال 98

        if (this.filterModel.monthId === MonthsEnum.Mehr) {  // نمایش دیتا های پایانه3 برای مهر 99
          this.chartData = new ChartModel();
          let years = data[2].Year;
          let month = years[1].month;
          this.chartData.date = month[0].date;
          this.chartData.chartData = month[0].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.aban) {  // نمایش دیتا های پایانه3 برای آبان 99
          this.chartData = new ChartModel();
          let years = data[2].Year;
          let month = years[1].month;
          this.chartData.date = month[1].date;
          this.chartData.chartData = month[1].chartData;
          this.apiService.subject.next(this.chartData);

        } else if (this.filterModel.monthId === MonthsEnum.azar) {  // نمایش دیتا های پایانه3 برای آذر 99
          this.chartData = new ChartModel();
          let years = data[2].Year;
          let month = years[1].month;
          this.chartData.date = month[2].date;
          this.chartData.chartData = month[2].chartData;
          this.apiService.subject.next(this.chartData);

        }
      }
    }
  }

  // تابعی که با تغییر هر کدام از فیلتر ها اجرا می شود
  changeFilter() {
    this.getChartData();
  }
}
