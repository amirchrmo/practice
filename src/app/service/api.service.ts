import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl = '/api/data';
  data$: Observable<any>;
  subject = new Subject<any>();

constructor(private http: HttpClient) { }

  getChartData() {
    this.data$ = this.http.get(this.baseUrl);
    return this.data$;
  }
}
